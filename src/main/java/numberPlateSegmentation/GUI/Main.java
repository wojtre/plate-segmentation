package numberPlateSegmentation.GUI;

import numberPlateSegmentation.imageProcessing.interfaces.FrameCaptureService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.awt.*;

/**
 * Created by Wojciech on 26-Nov-15.
 */
public class Main {
    private static final String OPENCV_PATH = "C:\\Program Files\\Java\\opencv-2.4.11\\opencv\\build\\java\\x64\\opencv_java2411.dll";

    public static void main(String[] args) {
        System.load(OPENCV_PATH);
        ApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("context.xml");
        FrameCaptureService frameCaptureService = classPathXmlApplicationContext.getBean("frameCaptureService",
                FrameCaptureService.class);
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MainWindow mainWindow
                            = new MainWindow();
                    frameCaptureService.setMainWindow(mainWindow);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        frameCaptureService.run();
//        long start = System.nanoTime();
//        frameCaptureService.capture("C:\\Users\\Wojciech\\javaWorkspace\\numberPlateRecognition\\filmy\\film2.avi");
//        long end = System.nanoTime();
//        System.out.println((end-start)/1000000000.0+" s");
    }
}
