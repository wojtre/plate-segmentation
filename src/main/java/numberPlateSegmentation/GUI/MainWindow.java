package numberPlateSegmentation.GUI;


import org.opencv.core.Point;
import org.opencv.core.Rect;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainWindow {

    public JFrame frame;
    private JPanel panelVideo;
    private JButton btnPlayStop;
    private JLabel lblOstatnioWydzieloneTablice;
    private JLabel lblWydzieloneZnaki;
    private JLabel lblPlate1;
    private JLabel lblPlate2;
    private JLabel lblPlate3;
    private boolean startStop;
    private JButton btnFile;
    private String fileName;
    private JTextField textFile;
    private java.util.List<JLabel> signs1;
    private java.util.List<JLabel> signs2;
    private java.util.List<JLabel> signs3;
    private JSlider videoSpeedSlide;
    private JLabel lblSlider;
    private Point startDrag;
    private Point endDrag;
    private Rect regionOfInterest;
    private JLabel labelVideo;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MainWindow window = new MainWindow();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public MainWindow() {
        initialize();
    }

    public void clickStop() {
        btnPlayStop.doClick();
    }

    public void setPlate(JLabel plate) {
        lblPlate3 = lblPlate2;
        lblPlate2 = lblPlate1;
        lblPlate1 = plate;
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        initializeFrame();
        addPanelVideo();
        addVideoLabel();
        addBtnPlayStop();
        addLblWydzieloneTablice();
        addLblWydzieloneZnaki();
        addPlates();
        addBtnFileChoose();
        addTxtFile();
        addSigns();
        addVideoSpeedSlider();
        addLblSlider();
        frame.setVisible(true);
    }

    private void addVideoLabel(){
        labelVideo= new JLabel();
        labelVideo.addMouseListener(new MouseAdapter() {
                                        @Override
                                        public void mousePressed(MouseEvent e) {
                                            startDrag = new Point(e.getX(), e.getY());
                                            endDrag = startDrag;
                                            regionOfInterest=null;
                                        }

                                        @Override
                                        public void mouseReleased(MouseEvent e) {
                                            regionOfInterest = new Rect(startDrag, new Point(e.getX(), e.getY()));
                                        }
                                    }
        );
        labelVideo.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                endDrag = new Point(e.getX(), e.getY());
            }
        });
        panelVideo.add(labelVideo);
    }

    private void addSigns() {
        signs1 = new ArrayList<JLabel>();
        signs2 = new ArrayList<JLabel>();
        signs3 = new ArrayList<JLabel>();
        for (int i = 0; i < 9; i++) {
            JLabel jLabel1 = new JLabel();
            jLabel1.setBounds(790 + i * 40, 400, 40, 40);
            signs1.add(jLabel1);
            frame.add(jLabel1);
            JLabel jLabel2 = new JLabel();
            jLabel2.setBounds(790 + i * 40, 460, 40, 40);
            signs2.add(jLabel2);
            frame.add(jLabel2);

            JLabel jLabel3 = new JLabel();
            jLabel3.setBounds(790 + i * 40, 520, 40, 40);
            signs3.add(jLabel3);
            frame.add(jLabel3);

        }
    }

    private void addPlates() {
        lblPlate1 = new JLabel();
        lblPlate1.setBounds(790, 270, 251, 70);
        frame.getContentPane().add(lblPlate1);

        lblPlate2 = new JLabel();
        lblPlate2.setBounds(950, 270, 251, 70);
        frame.getContentPane().add(lblPlate2);

        lblPlate3 = new JLabel();
        lblPlate3.setBounds(1110, 270, 251, 70);
        frame.getContentPane().add(lblPlate3);
    }

    private void addLblWydzieloneZnaki() {
        lblWydzieloneZnaki = new JLabel("Wydzielone znaki");
        lblWydzieloneZnaki.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblWydzieloneZnaki.setBounds(790, 350, 125, 32);
        frame.getContentPane().add(lblWydzieloneZnaki);
    }

    private void addLblWydzieloneTablice() {
        lblOstatnioWydzieloneTablice = new JLabel(
                "Ostatnio wydzielone tablice");
        lblOstatnioWydzieloneTablice.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblOstatnioWydzieloneTablice.setBounds(790, 240, 193, 14);
        frame.getContentPane().add(lblOstatnioWydzieloneTablice);
    }

    private void addBtnPlayStop() {
        btnPlayStop = new JButton("Play");
        btnPlayStop.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                if (startStop) {
                    startStop = false;
                    btnPlayStop.setText("Play");
                } else {
                    startStop = true;
                    btnPlayStop.setText("Stop");
                }
            }
        });
        btnPlayStop.setBounds(320, 612, 89, 30);
        frame.getContentPane().add(btnPlayStop);
    }

    private void addPanelVideo() {
        panelVideo = new JPanel();
        panelVideo.setBounds(24, 25, 731, 537);
        frame.getContentPane().add(panelVideo);
    }

    private Rectangle2D.Float makeRectangle(int x1, int y1, int x2, int y2) {
        return new Rectangle2D.Float(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x1 - x2), Math.abs(y1 - y2));
    }

    private void initializeFrame() {
        frame = new JFrame();
        frame.setBounds(100, 100, 1295, 703);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        try {
            // Set System L&F
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException e) {
            // handle exception
        } catch (ClassNotFoundException e) {
            // handle exception
        } catch (InstantiationException e) {
            // handle exception
        } catch (IllegalAccessException e) {
            // handle exception
        }
    }

    private void addTxtFile() {
        textFile = new JTextField("Plik wideo");
        textFile.setBounds(790, 25, 250, 30);
        frame.getContentPane().add(textFile);
    }

    private void addBtnFileChoose() {
        btnFile = new JButton("Wybierz plik");
        btnFile.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
            try {
                fileChooser.showOpenDialog(null);
                File f = fileChooser.getSelectedFile();
                fileName = f.getAbsolutePath();
                textFile.setText(fileName);
            } catch (Exception ex) {

            }
        });
        btnFile.setBounds(1050, 25, 89, 30);
        frame.getContentPane().add(btnFile);
    }

    private void addLblSlider() {
        lblSlider = new JLabel("Predkosc filmu");
        lblSlider.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblSlider.setBounds(790, 110, 200, 26);
        frame.add(lblSlider);
    }

    private void addVideoSpeedSlider() {
        videoSpeedSlide = new JSlider();
        videoSpeedSlide.setBounds(790, 150, 200, 26);
        videoSpeedSlide.setValue(30);
        videoSpeedSlide.setMaximum(250);
        videoSpeedSlide.setMinimum(10);
        videoSpeedSlide.setPaintTicks(true);
        frame.getContentPane().add(videoSpeedSlide);
    }


    public JLabel getLblPlate1() {
        return lblPlate1;
    }

    public void setLblPlate1(JLabel lblPlate1) {
        this.lblPlate1 = lblPlate1;
    }

    public JLabel getLblPlate2() {
        return lblPlate2;
    }

    public void setLblPlate2(JLabel lblPlate2) {
        this.lblPlate2 = lblPlate2;
    }

    public JLabel getLblPlate3() {
        return lblPlate3;
    }

    public void setLblPlate3(JLabel lblPlate3) {
        this.lblPlate3 = lblPlate3;
    }

    public JPanel getPanelVideo() {
        return panelVideo;
    }

    public void setPanelVideo(JPanel panelVideo) {
        this.panelVideo = panelVideo;
    }

    public boolean isStartStop() {
        return startStop;
    }

    public String getFileName() {
        return textFile.getText();
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public java.util.List<JLabel> getSigns1() {
        return signs1;
    }

    public void setSigns1(List<JLabel> signs1) {
        this.signs1 = signs1;
    }

    public List<JLabel> getSigns2() {
        return signs2;
    }

    public void setSigns2(List<JLabel> signs2) {
        this.signs2 = signs2;
    }

    public List<JLabel> getSigns3() {
        return signs3;
    }

    public void setSigns3(List<JLabel> signs3) {
        this.signs3 = signs3;
    }

    public int getVideoSpeed() {
        return this.videoSpeedSlide.getValue();
    }

    public Point getStartDrag() {
        return startDrag;
    }

    public Point getEndDrag() {
        return endDrag;
    }

    public Rect getRegionOfInterest() {
        return regionOfInterest;
    }

    public JLabel getLabelVideo() {
        return labelVideo;
    }
}

