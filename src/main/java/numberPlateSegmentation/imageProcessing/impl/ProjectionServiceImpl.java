package numberPlateSegmentation.imageProcessing.impl;

import numberPlateSegmentation.imageProcessing.interfaces.ProjectionService;
import org.opencv.core.Mat;
import utils.ChartUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wojciech on 10-Oct-15.
 */

public class ProjectionServiceImpl implements ProjectionService {
    public static final double PEAK_CONST = 0.86;
    public static final double TRUNCATE_BOUND_RATIO = 0.2;


    private long maxValue = 0;
    private int indexOfMaxValue = 0;
    private int averageValue = 0;
    private final double boundConst = 0.7;

    @Override
    public List<Integer> findPlacesOfPartition(Mat plate) {
        List<Integer> placesOfPartition = new ArrayList<Integer>();
        long[] projectionOnAxis = projectOnAxisXBinary(plate);
        ChartUtils.printProjectionXFromArray(projectionOnAxis);
        findMaxValueAndItsIndex(projectionOnAxis);
        calculateAverageValue(projectionOnAxis);
        while (true) {
            int tmpIndexOfMax = returnsIndexOfMax(projectionOnAxis);
            int leftBound = findLeftBoundIndex(tmpIndexOfMax, projectionOnAxis, boundConst);
            int rightBound = findRightBoundIndex(tmpIndexOfMax, projectionOnAxis, boundConst);
            if (!hasMorePlacesOfPartition(projectionOnAxis[tmpIndexOfMax], PEAK_CONST)) {
                return placesOfPartition;
            }
            zeroizeIntervalOnProjection(leftBound, rightBound, projectionOnAxis);
            placesOfPartition.add(new Integer(tmpIndexOfMax));
        }
    }
    @Override
    public List<Integer> findPlacesOfTruncation(long[] derivative) {
        List<Integer> placesOfPartition = new ArrayList<>();
        findMaxValueAndItsIndex(derivative);
        calculateAverageValue(derivative);
        while (true) {
            int tmpIndexOfMax = returnsIndexOfMax(derivative);
            int leftBound = findLeftBoundIndex(tmpIndexOfMax, derivative, TRUNCATE_BOUND_RATIO);
            int rightBound = findRightBoundIndex(tmpIndexOfMax, derivative, TRUNCATE_BOUND_RATIO);
            if (!hasMorePlacesOfPartition(derivative[tmpIndexOfMax],0.6)) {
                return placesOfPartition;
            }
            zeroizeIntervalOnProjection(leftBound, rightBound, derivative);
            placesOfPartition.add(new Integer(tmpIndexOfMax));
            if(placesOfPartition.size()>1){
                return placesOfPartition;
            }
        }
    }

    private boolean hasMorePlacesOfPartition(long projectionOnAxi, double peakRatio) {
        return projectionOnAxi > (long) (maxValue * peakRatio);
    }

    @Override
    public long[] projectOnAxisXBinary(Mat mat) {
        long[] projection = new long[mat.cols()];
        for (int i = 0; i < mat.cols(); i++) {
            for (int j = 0; j < mat.rows(); j++) {
                if (mat.get(j, i)[0] > 0) {
                    projection[i]++;
                }
            }
        }
        return projection;
    }

    @Override
    public long[] projectOnAxisX(Mat mat) {
        long[] projection = new long[mat.cols()];
        for (int i = 0; i < mat.cols(); i++) {
            for (int j = 0; j < mat.rows(); j++) {
                projection[i] += mat.get(j, i)[0];
            }
        }
        return projection;
    }

    @Override
    public int[] projectOnAxisYBinary(Mat mat) {
        int[] projection = new int[mat.rows()];
        for (int i = 0; i < mat.rows(); i++) {
            for (int j = 0; j < mat.cols(); j++) {
                if (mat.get(i, j)[0] > 0) {
                    projection[i]++;
                }
            }
        }
        return projection;
    }

    @Override
    public long[] projectOnAxisY(Mat mat) {
        long[] projection = new long[mat.rows()];
        for (int i = 0; i < mat.rows(); i++) {
            for (int j = 0; j < mat.cols(); j++) {
                projection[i] += mat.get(i, j)[0];
            }
        }
        return projection;
    }

    private void calculateAverageValue(long[] projectionOnAxis) {
        int sum = 0;
        for (long i : projectionOnAxis) {
            sum += i;
        }
        averageValue = (int) (sum / (double) projectionOnAxis.length);
    }

    private void findMaxValueAndItsIndex(long[] projectionOnAxis) {
        long max = 0;
        int index = 0;
        for (int i = 0; i < projectionOnAxis.length; i++) {
            if (max < projectionOnAxis[i]) {
                max = projectionOnAxis[i];
                index = i;
            }
        }
        maxValue = max;
        indexOfMaxValue = index;
    }

    private int returnsIndexOfMax(long[] projectionOnAxis) {
        long max = 0;
        int index = 0;
        for (int i = 0; i < projectionOnAxis.length; i++) {
            if (max < projectionOnAxis[i]) {
                max = projectionOnAxis[i];
                index = i;
            }
        }
        return index;
    }

    private int findLeftBoundIndex(int indexOfMax, long[] projectionOnAxis,double boundRatio) {
        for (int i = indexOfMax; i >= 0; i--) {
            if (projectionOnAxis[i] < (int) (projectionOnAxis[indexOfMax] * boundRatio)) {
                return i;
            }
        }
        return 0;
    }

    private int findRightBoundIndex(int indexOfMax, long[] projectionOnAxis,double boundRatio) {
        for (int i = indexOfMax; i < projectionOnAxis.length; i++) {
            if (projectionOnAxis[i] < (int) (projectionOnAxis[indexOfMax] * boundRatio)) {
                return i;
            }
        }
        return projectionOnAxis.length - 1;
    }

    private void zeroizeIntervalOnProjection(int left, int right, long[] projectionOnAxis) {
        for (int i = left; i <= right; i++) {
            projectionOnAxis[i] = 0;
        }
    }

}

