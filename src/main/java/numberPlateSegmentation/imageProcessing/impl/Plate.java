package numberPlateSegmentation.imageProcessing.impl;

import utils.ImageUtils;

import java.util.List;

/**
 * Created by Wojciech on 31-Oct-15.
 */
public class Plate {
    private List<Sign> signs;

    public Plate(List<Sign> signs) {
        this.signs = signs;
    }

    public Plate() {
    }

    public List<Sign> getSigns() {
        return signs;
    }

    public void setSigns(List<Sign> signs) {
        this.signs = signs;
    }

    public void displaySigns() {
        for (Sign sign : signs) {
            ImageUtils.displayImage(ImageUtils.mat2BufferedImage(sign.getSignImageInMat()));
        }
    }
}
