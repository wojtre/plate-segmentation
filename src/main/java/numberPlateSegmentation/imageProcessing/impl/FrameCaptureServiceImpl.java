package numberPlateSegmentation.imageProcessing.impl;

import com.xuggle.xuggler.Global;
import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IPacket;
import com.xuggle.xuggler.IPixelFormat;
import com.xuggle.xuggler.IStream;
import com.xuggle.xuggler.IStreamCoder;
import com.xuggle.xuggler.IVideoPicture;
import com.xuggle.xuggler.IVideoResampler;
import com.xuggle.xuggler.Utils;
import numberPlateSegmentation.GUI.MainWindow;
import numberPlateSegmentation.imageProcessing.interfaces.FrameCaptureService;
import numberPlateSegmentation.imageProcessing.interfaces.PlateSegmentationService;
import numberPlateSegmentation.imageProcessing.interfaces.SegmentationCompositeService;
import org.opencv.highgui.Highgui;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by Wojciech on 10-Oct-15.
 */
public class FrameCaptureServiceImpl implements FrameCaptureService {
    private static final double SECONDS_BETWEEN_FRAMES = 0.03;
    private static final long NANO_SECONDS_BETWEEN_FRAMES =
            (long) (Global.DEFAULT_PTS_PER_SECOND * SECONDS_BETWEEN_FRAMES);
    private static int SLEEP_TIME = 20;
    private long lastFrameTime = Global.NO_PTS;
    private long processingTime = 0;
    private PlateSegmentationService plateSegmentationService;
    private SegmentationCompositeService segmentationCompositeService;
    private MainWindow mainWindow;
    private String oldFileName = "";

    @Override
    public void run() {

        while (true) {
            while (!mainWindow.isStartStop()) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            plateSegmentationService.setPanel(mainWindow.getPanelVideo());
            plateSegmentationService.setMainWindow(mainWindow);
            segmentationCompositeService.setMainWindow(mainWindow);
            capture(mainWindow.getFileName());
        }
    }

    @Override
    public void capture(String filename) {
        if (!oldFileName.equals(filename)) {
            lastFrameTime = Global.NO_PTS;
            processingTime = 0;
        }
        oldFileName = filename;
        validateIVideoResamplerIsSupported();
        IContainer container = null;
        try {
            container = getContainer(filename);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(new JFrame(), "Wybrano niewlasciwy plik!", "Wystapil blad", JOptionPane.ERROR_MESSAGE);
            mainWindow.clickStop();
            return;
        }
        int videoStreamId = getFirstVideoStreamId(container);
        IStreamCoder videoCoder = getStreamCoder(container, videoStreamId);
        openVideoCoder(videoCoder);
        IVideoResampler resampler = getVideoResampler(videoCoder);
        IPacket packet = IPacket.make();
        while (container.readNextPacket(packet) >= 0) {

            // Now we have a packet, let's see if it belongs to our video strea
            if (doesPacketBelongToStream(videoStreamId, packet)) {
                // We allocate a new picture to get the data out of Xuggle

                IVideoPicture picture = IVideoPicture.make(videoCoder.getPixelType(),
                        videoCoder.getWidth(), videoCoder.getHeight());
                int offset = 0;
                while (offset < packet.getSize()) {
                    int bytesDecoded = videoCoder.decodeVideo(picture, packet, offset);
                    if (bytesDecoded < 0)
                        throw new RuntimeException("got error decoding video in: " + filename);
                    offset += bytesDecoded;
                    if (picture.isComplete()) {
                        captureFrameIfTimePassed(resampler, picture);
                    }
                }
            }
            while (!mainWindow.isStartStop()) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            SLEEP_TIME = mainWindow.getVideoSpeed();
            if (!oldFileName.equals(mainWindow.getFileName())) {
                return;
            }
        }
        closeResources(container, videoCoder);
    }

    private void processFrame(BufferedImage image) {
        try {
//            ImageUtils.displayImage(image);
            System.out.println((System.nanoTime() - processingTime) / 1000000.0 + " ms");
            processingTime = System.nanoTime();
            long start = System.nanoTime();
            File outputfile = new File("tmp.jpg");
            ImageIO.write(image, "jpg", outputfile);
            segmentationCompositeService.segmentSign((Highgui.imread(outputfile.getAbsolutePath(), Highgui.CV_LOAD_IMAGE_GRAYSCALE)));
            long end = System.nanoTime();
            System.out.println("\n\n plate  " + (end - start) / 1000000.0 + " ms");
            Thread.sleep(SLEEP_TIME);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean doesPacketBelongToStream(int videoStreamId, IPacket packet) {
        return packet.getStreamIndex() == videoStreamId;
    }

    private void openVideoCoder(IStreamCoder videoCoder) {
        if (videoCoder.open(null, null) < 0)
            throw new RuntimeException(
                    "could not open video decoder for container");
    }

    private IVideoResampler getVideoResampler(IStreamCoder videoCoder) {
        IVideoResampler resampler = null;
        if (videoCoder.getPixelType() != IPixelFormat.Type.BGR24) {
            resampler = IVideoResampler.make(
                    videoCoder.getWidth(), videoCoder.getHeight(), IPixelFormat.Type.BGR24,
                    videoCoder.getWidth(), videoCoder.getHeight(), videoCoder.getPixelType());
            if (resampler == null)
                throw new RuntimeException(
                        "could not create color space resampler  ");
        }
        return resampler;
    }

    private IStreamCoder getStreamCoder(IContainer container, int videoStreamId) {
        IStream stream = container.getStream(videoStreamId);

        return stream.getStreamCoder();
    }

    private IContainer getContainer(String filename) {
        IContainer container = IContainer.make();

        if (container.open(filename, IContainer.Type.READ, null) < 0)
            throw new IllegalArgumentException("could not open file: " + filename);
        return container;
    }

    private int getFirstVideoStreamId(IContainer container) {
        int videoStreamId = -1;
        int numStreams = container.getNumStreams();
        for (int i = 0; i < numStreams; i++) {
            IStreamCoder coder = getStreamCoder(container, i);
            if (coder.getCodecType() == ICodec.Type.CODEC_TYPE_VIDEO) {
                videoStreamId = i;
                break;
            }
        }
        validateVideoStream(videoStreamId);
        return videoStreamId;
    }

    private void validateVideoStream(int videoStreamId) {
        if (videoStreamId == -1)
            throw new RuntimeException("could not find video stream in container ");
    }

    private void closeResources(IContainer container, IStreamCoder videoCoder) {
        if (videoCoder != null) {
            videoCoder.close();
        }
        if (container != null) {
            container.close();
        }
    }

    private void captureFrameIfTimePassed(IVideoResampler resampler, IVideoPicture picture) {
        calculateTimeForFirstFrame(picture);
        if (shouldCaptureFrame(picture)) {
            IVideoPicture newPic = picture;
            lastFrameTime += NANO_SECONDS_BETWEEN_FRAMES;
            if (isVideoInBGR24Format(resampler)) {
                newPic = convertVideoToBGR24Format(resampler, picture);
            }
            BufferedImage javaImage = Utils.videoPictureToImage(newPic);
            processFrame(javaImage);
        }
    }

    private void calculateTimeForFirstFrame(IVideoPicture picture) {
        if (lastFrameTime == Global.NO_PTS) {
            lastFrameTime = picture.getPts() - NANO_SECONDS_BETWEEN_FRAMES;
        }
    }

    private boolean shouldCaptureFrame(IVideoPicture picture) {
        return picture.getPts() - lastFrameTime >= NANO_SECONDS_BETWEEN_FRAMES;
    }

    private void validateIVideoResamplerIsSupported() {
        if (!IVideoResampler.isSupported(
                IVideoResampler.Feature.FEATURE_COLORSPACECONVERSION))
            throw new RuntimeException(
                    "you must install the GPL version of Xuggler (with IVideoResampler" +
                            " support) for this demo to work");
    }

    private IVideoPicture convertVideoToBGR24Format(IVideoResampler resampler, IVideoPicture picture) {
        IVideoPicture newPic;
        newPic = IVideoPicture.make(
                resampler.getOutputPixelFormat(), picture.getWidth(),
                picture.getHeight());
        if (resampler.resample(newPic, picture) < 0) {
            throw new RuntimeException(
                    "could not resample video ");
        }
        if (newPic.getPixelType() != IPixelFormat.Type.BGR24) {
            throw new RuntimeException(
                    "could not decode video as BGR 24 bit data ");
        }
        return newPic;
    }

    private boolean isVideoInBGR24Format(IVideoResampler resampler) {
        return resampler != null;
    }

    public MainWindow getMainWindow() {
        return mainWindow;
    }

    @Override
    public void setMainWindow(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }

    // Spring injections
    public void setPlateSegmentationService(PlateSegmentationService plateSegmentationService) {
        this.plateSegmentationService = plateSegmentationService;
    }

    public void setSegmentationCompositeService(SegmentationCompositeService segmentationCompositeService) {
        this.segmentationCompositeService = segmentationCompositeService;
    }
}
