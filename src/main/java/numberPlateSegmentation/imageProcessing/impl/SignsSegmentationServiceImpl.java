package numberPlateSegmentation.imageProcessing.impl;

import numberPlateSegmentation.imageProcessing.interfaces.PlateTruncationService;
import numberPlateSegmentation.imageProcessing.interfaces.ProjectionService;
import numberPlateSegmentation.imageProcessing.interfaces.SignsSegmentationService;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import utils.ImageUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Wojciech on 06-Oct-15.
 */
public class SignsSegmentationServiceImpl implements SignsSegmentationService {
    private static final int MIN_SIGNS_WIDTH = 10;
    private ProjectionService projectionService;
    private PlateTruncationService plateTruncationService;


    @Override
    public List<Mat> separateSigns(Mat plateImage) {
        ImageUtils.resizeImage(plateImage, 200);
        plateImage = plateTruncationService.truncatePlate(plateImage);
//        Highgui.imwrite("C:\\Users\\Wojciech\\Google Drive\\studia\\inz\\images\\segmentacja_rzut\\przycieta_tablica.jpg", plateImage);

        prepareImage(plateImage);
        plateImage.size();
        List<Integer> placesOfPartition = projectionService.findPlacesOfPartition(plateImage);
        List<Sign> separatedSigns = extractSignsFromPlate(placesOfPartition, plateImage);
        truncateSigns(separatedSigns);
        removeNotValidateSigns(separatedSigns, plateImage.size());
        ArrayList<Mat> signs = new ArrayList<Mat>();
        for (int i = 0; i < separatedSigns.size(); i++) {
            Mat signImageInMat = separatedSigns.get(i).getSignImageInMat();

//            Highgui.imwrite("C:\\Users\\Wojciech\\Google Drive\\studia\\inz\\images\\segmentacja_rzut\\znak" + i + ".jpg", signImageInMat);

            signs.add(signImageInMat);
        }
        return signs;
    }

    private List<Sign> extractSignsFromPlate(List<Integer> placesOfPartition, Mat plateImage) {
        Collections.sort(placesOfPartition);
        Iterator<Integer> it = placesOfPartition.iterator();
        List<Sign> separatedSigns = new ArrayList<>();
        Integer left = 0, right = 0;
        while (it.hasNext()) {
            right = it.next();
            if (isWidthEnought(left, right)) {
                separatedSigns.add(new Sign(cutArea(left, right, 0,
                        plateImage.rows(), plateImage)));
            }
            left = right;
        }
        if (isWidthEnought(right, plateImage.cols()-1)) {
            separatedSigns.add(new Sign(cutArea(right, plateImage.cols() - 1, 0, plateImage.rows(), plateImage)));
        }
        return separatedSigns;
    }

    private boolean isWidthEnought(Integer left, Integer right) {
        return right - left > MIN_SIGNS_WIDTH;
    }

    private void truncateSigns(List<Sign> separatedSigns) {
        for (int i = 0; i < separatedSigns.size(); i++) {
            separatedSigns.get(i).truncateSign();
        }
    }

    private void removeNotValidateSigns(List<Sign> separatedSigns, Size plateSize) {
        Iterator<Sign> iter = separatedSigns.iterator();
        while (iter.hasNext()) {
            if (!iter.next().isValid(plateSize)) {
                iter.remove();
            }
        }
    }

    private Mat cutArea(int left, int right, int up, int down, Mat plateImage) {
        if (left >= right) {
            return null;
        }
        Mat newImage = new Mat(down - up, right - left, plateImage.type());
        plateImage.submat(up, down, left, right).copyTo(newImage);
        return newImage;
    }

    private void prepareImage(Mat plateImage) {
//        ImageUtils.displayImage(plateImage);


        Imgproc.adaptiveThreshold(plateImage, plateImage, 255,
                Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY,
                1 + (int) (plateImage.cols() / 16.0) * 2, 1);
//        ImageUtils.displayImage(plateImage);
//        Highgui.imwrite("C:\\Users\\Wojciech\\Google Drive\\studia\\inz\\images\\segmentacja_rzut\\binaryzacja.jpg", plateImage);

    }

    // SPRING INJECTIONS

    public void setProjectionService(ProjectionService projectionService) {
        this.projectionService = projectionService;
    }

    public void setPlateTruncationService(PlateTruncationService plateTruncationService) {
        this.plateTruncationService = plateTruncationService;
    }
}
