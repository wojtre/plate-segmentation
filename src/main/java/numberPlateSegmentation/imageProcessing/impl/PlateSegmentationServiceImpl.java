package numberPlateSegmentation.imageProcessing.impl;

import numberPlateSegmentation.GUI.MainWindow;
import numberPlateSegmentation.imageProcessing.interfaces.PlateSegmentationService;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import utils.ImageUtils;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Wojciech on 18-Oct-15.
 */
public class PlateSegmentationServiceImpl implements PlateSegmentationService {

    private static final double HIGHEST_RATIO = 0.5;
    private static final double LOWEST_RATIO = 0.12;
    private static final int IMAGE_WIDTH = 650;
    private static final int THRESH = 230;
    private static final int MIN_EDGES_NR = 14;
    private static final int MAX_HEIGHT = 50;
    private static final int MIN_HEIGHT = 5;
    private static final int MAX_WIDTH = 100;
    private static final int MIN_WIDTH = 20;
    private static final int MAX_EDGES_NR = 30;
    private static final double SIZE_HORIZONTAL_EXPAND_RATIO = 0.01;
    private static final double SIZE_VERTICAL_EXPAND_RATIO = 0.25;
    private MainWindow mainWindow;
    private JPanel panel;

    @Override
    public List<Mat> segmentPlatesFromImage(Mat image) {
        resizeImage(image, IMAGE_WIDTH);
//        Highgui.imwrite("C:\\Users\\Wojciech\\Google Drive\\studia\\inz\\images\\segmentacja\\oryginal.jpg", equalizedImage);
        Mat equalizedImage = new Mat();
        Imgproc.equalizeHist(image, equalizedImage);
        Mat binarizedImageWithEdges = getBinarizedImageWithEdges(equalizedImage);
        Mat imageWithProbablyPlateArea = localizeProbablyPlateArea(binarizedImageWithEdges);
//        ImageUtils.displayImage(ImageUtils.mat2BufferedImage(imageWithProbablyPlateArea));
        List<Rect> rectanglesOfPossiblePlateAreas = getRectanglesOfPossiblePlateAreas(imageWithProbablyPlateArea);
        removeNotValidPlates(equalizedImage, binarizedImageWithEdges, rectanglesOfPossiblePlateAreas);
        Mat drawRectangles = drawRectangles(image, rectanglesOfPossiblePlateAreas);
        drawRegionOfInterest(drawRectangles, mainWindow.getStartDrag(), mainWindow.getEndDrag());
        ImageUtils.displayImageOnFrame(ImageUtils.mat2BufferedImage(drawRectangles), panel, mainWindow.getLabelVideo());
        return getAllRectsFromImage(equalizedImage, rectanglesOfPossiblePlateAreas);
    }

    private void drawRegionOfInterest(Mat image, Point start, Point end) {

        if (start != null && end != null) {
            drawBlueRectangle(image, new Rect(start, end));
        }
    }

    private void removeNotValidPlates(Mat image, Mat binarizedImageWithEdges, List<Rect> rectanglesOfPossiblePlateAreas) {
        Mat drawRectangles = drawRectangles(image, rectanglesOfPossiblePlateAreas);
//        Highgui.imwrite("C:\\Users\\Wojciech\\Google Drive\\studia\\inz\\images\\segmentacja\\obszary_bez_walidacji.jpg", drawRectangles);

//        ImageUtils.displayImage(ImageUtils.mat2BufferedImage(drawRectangles));
        removePlateAreasWithNoProperRatio(rectanglesOfPossiblePlateAreas);

        drawRectangles = drawRectangles(image, rectanglesOfPossiblePlateAreas);
//        Highgui.imwrite("C:\\Users\\Wojciech\\Google Drive\\studia\\inz\\images\\segmentacja\\walidacja_proporcji.jpg", drawRectangles);

//        ImageUtils.displayImage(ImageUtils.mat2BufferedImage(drawRectangles));
        removePlateAreasWithNoProperSize(rectanglesOfPossiblePlateAreas);

        drawRectangles = drawRectangles(image, rectanglesOfPossiblePlateAreas);
//        Highgui.imwrite("C:\\Users\\Wojciech\\Google Drive\\studia\\inz\\images\\segmentacja\\walidacja_rozmiaru.jpg", drawRectangles);
//        ImageUtils.displayImage(ImageUtils.mat2BufferedImage(drawRectangles));
        removePlateAreasWithNoProperEdgesNumber(rectanglesOfPossiblePlateAreas, binarizedImageWithEdges);
        drawRectangles = drawRectangles(image, rectanglesOfPossiblePlateAreas);
//        Highgui.imwrite("C:\\Users\\Wojciech\\Google Drive\\studia\\inz\\images\\segmentacja\\walidacja_ilosci_krawedzi.jpg", drawRectangles);
        removePlateNotFromROI(rectanglesOfPossiblePlateAreas);
    }

    private void resizeImage(Mat imageToResize, double width) {
        double ratio = countRatioOfImage(imageToResize);
        double height = ratio * width;
        Imgproc.resize(imageToResize, imageToResize, new Size(width, height));
    }

    private double countRatioOfImage(final Mat image) {
        return image.height() / (double) image.width();
    }

    private Mat drawRectangles(Mat image, List<Rect> rects) {
        Mat imageWithDrawedRects = new Mat();
        Imgproc.cvtColor(image, imageWithDrawedRects, Imgproc.COLOR_GRAY2BGR);
        for (int i = 0; i < rects.size(); i++) {
            Rect rect = rects.get(i);
            drawRectangle(imageWithDrawedRects, rect);
        }
        return imageWithDrawedRects;
    }

    private void drawRectangle(Mat imageWithDrawedRects, Rect rect) {
        Core.rectangle(imageWithDrawedRects, rect.tl(), rect.br(), new Scalar(0, 0, 255, 255), 2);
    }

    private void drawBlueRectangle(Mat imageWithDrawedRects, Rect rect) {
        Core.rectangle(imageWithDrawedRects, rect.tl(), rect.br(), new Scalar(255, 0, 0, 255), 2);
    }

    public Mat localizeProbablyPlateArea(Mat image) {
        Mat edges = new Mat();
        image.copyTo(edges);
        int erosion_size_y = 12;
        int erosion_size_x = 2;
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2 * erosion_size_y + 1, 2 * erosion_size_x + 1));
        close(edges, element);
//        Highgui.imwrite("C:\\Users\\Wojciech\\Google Drive\\studia\\inz\\images\\segmentacja\\segmentacja_zamkniecie.jpg", edges);


        erosion_size_y = 2;
        erosion_size_x = 2;
        element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2 * erosion_size_y + 1, 2 * erosion_size_x + 1));
        open(edges, element);
//        Highgui.imwrite("C:\\Users\\Wojciech\\Google Drive\\studia\\inz\\images\\segmentacja\\segmentacja_otwarcie.jpg", edges);

        return edges;
    }

    private Mat getBinarizedImageWithEdges(Mat image) {
        Mat imageWithAreas = new Mat();
//        ImageUtils.displayImage(ImageUtils.mat2BufferedImage(image));
        image.copyTo(imageWithAreas);
//        Highgui.imwrite("C:\\Users\\Wojciech\\Google Drive\\studia\\inz\\images\\segmentacja\\obraz_hist_wyrownany.jpg", imageWithAreas);
//        ImageUtils.displayImage(ImageUtils.mat2BufferedImage(imageWithAreas));

        imageWithAreas = ImageUtils.detectVerticalEdges(imageWithAreas);
//        Highgui.imwrite("C:\\Users\\Wojciech\\Google Drive\\studia\\inz\\images\\segmentacja\\krawedzie_bez_binar.jpg", imageWithAreas);

//        ImageUtils.displayImage(ImageUtils.mat2BufferedImage(imageWithAreas));
        Imgproc.threshold(imageWithAreas, imageWithAreas, THRESH, 255, Imgproc.THRESH_BINARY);
//        Highgui.imwrite("C:\\Users\\Wojciech\\Google Drive\\studia\\inz\\images\\segmentacja\\krawedzie_binar.jpg", imageWithAreas);

//        ImageUtils.displayImage(ImageUtils.mat2BufferedImage(imageWithAreas));

        return imageWithAreas;
    }

    private void removePlateAreasWithNoProperSize(List<Rect> rects) {
        Iterator<Rect> iterator = rects.iterator();
        while (iterator.hasNext()) {
            Rect rect = iterator.next();
            if (!hasProperSize(rect.size())) {
                iterator.remove();
            }
        }
    }

    private boolean hasProperSize(Size size) {
        double height = size.height;
        double width = size.width;
        return height < MAX_HEIGHT && height > MIN_HEIGHT && width < MAX_WIDTH && width > MIN_WIDTH;
    }

    private void removePlateAreasWithNoProperRatio(List<Rect> rects) {
        Iterator<Rect> iterator = rects.iterator();
        while (iterator.hasNext()) {
            Rect rect = iterator.next();
            double ratio = rect.height / (double) rect.width;
            if (!hasProperRatio(ratio)) {
                iterator.remove();
            }
        }
    }

    private void removePlateAreasWithNoProperEdgesNumber(List<Rect> rects, Mat imageOfEdges) {
        Iterator<Rect> iterator = rects.iterator();
        int middleEdges, thirdEdges, twoThirdEdges;
        while (iterator.hasNext()) {
            Rect rect = iterator.next();
            middleEdges = calculateVerticalEdgesInArea(imageOfEdges.submat(rect), rect.height / 2);
            thirdEdges = calculateVerticalEdgesInArea(imageOfEdges.submat(rect), rect.height / 3);
            twoThirdEdges = calculateVerticalEdgesInArea(imageOfEdges.submat(rect), 2 * rect.height / 3);
            if (!areEnoughtEdges(middleEdges, thirdEdges, twoThirdEdges)) {
                iterator.remove();
            }
        }
    }

    private void removePlateNotFromROI(List<Rect> rects) {
        if (mainWindow.getRegionOfInterest() == null) {
            return;
        }
        Iterator<Rect> iterator = rects.iterator();
        while (iterator.hasNext()) {
            Rect next = iterator.next();
            if (!(mainWindow.getRegionOfInterest().contains(next.br()) && mainWindow.getRegionOfInterest().contains(next.tl()))) {
                iterator.remove();
            }
        }
    }


    private boolean areEnoughtEdges(int... edgesNumber) {
        int vote = 0;
        for (int edge : edgesNumber) {
            if (edge > MIN_EDGES_NR && edge < MAX_EDGES_NR) {
                vote++;
            }
        }
        return vote > edgesNumber.length / 2;
    }

    private int calculateVerticalEdgesInArea(final Mat image, final int row) {
        double[] first = image.get(row, 0);
        int edges = 0;
        for (int col = 1; col < image.cols(); col++) {
            double[] second = image.get(row, col);
            if (first[0] != second[0]) {
                edges++;
            }
            first = second;
        }
        return edges;
    }

    private List<Mat> getAllRectsFromImage(Mat image, List<Rect> rects) {
        List<Mat> cutRects = new ArrayList<>();
        for (int i = 0; i < rects.size(); i++) {
            Rect smallRect = rects.get(i);
            cutRects.add(image.submat(expandRect(smallRect, image.size())));
        }
        return cutRects;
    }

    private Rect expandRect(Rect smallRect, Size imageSize) {
        double[] points = new double[4];
        double newX = smallRect.x - smallRect.width * SIZE_HORIZONTAL_EXPAND_RATIO;
        double newY = smallRect.y - smallRect.height * SIZE_VERTICAL_EXPAND_RATIO;
        points[0] = Math.max(newX, 0);
        points[1] = Math.max(newY, 0);
        points[2] = Math.min(smallRect.width * (1 + SIZE_HORIZONTAL_EXPAND_RATIO * 2), imageSize.width - 1 - points[0]);
        points[3] = Math.min(smallRect.height * (1 + SIZE_VERTICAL_EXPAND_RATIO * 2), imageSize.height - 1 - points[1]);
        return new Rect(points);
    }

    private boolean hasProperRatio(double ratio) {
        return ratio > LOWEST_RATIO && ratio < HIGHEST_RATIO;
    }

    private List<Rect> getRectanglesOfPossiblePlateAreas(Mat imageWithProbablyPlateAreas) {
        List<MatOfPoint> contours = findImageContours(imageWithProbablyPlateAreas);
        ArrayList<Rect> rects = new ArrayList<>();
        for (int i = 0; i < contours.size(); i++) {
            rects.add(Imgproc.boundingRect(contours.get(i)));
        }
        return rects;
    }

    private List<MatOfPoint> findImageContours(Mat image) {
        Mat copy = new Mat();
        image.copyTo(copy);
        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(copy, contours, new Mat(), Imgproc.RETR_CCOMP,
                Imgproc.CHAIN_APPROX_NONE);
        return contours;
    }

    private void open(Mat image, Mat element) {
        Imgproc.erode(image, image, element);
        Imgproc.dilate(image, image, element);
    }

    private void close(Mat image, Mat element) {
        Imgproc.dilate(image, image, element);
        Imgproc.erode(image, image, element);
    }

    @Override
    public void setMainWindow(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }

    @Override
    public void setPanel(JPanel panel) {
        this.panel = panel;
    }
}
