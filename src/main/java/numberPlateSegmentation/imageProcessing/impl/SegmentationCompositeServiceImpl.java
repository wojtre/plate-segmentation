package numberPlateSegmentation.imageProcessing.impl;

import numberPlateSegmentation.GUI.MainWindow;
import numberPlateSegmentation.imageProcessing.interfaces.NormalizationService;
import numberPlateSegmentation.imageProcessing.interfaces.PlateSegmentationService;
import numberPlateSegmentation.imageProcessing.interfaces.SegmentationCompositeService;
import numberPlateSegmentation.imageProcessing.interfaces.SignsSegmentationService;
import org.opencv.core.Mat;
import utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wojciech on 31-Oct-15.
 */
public class SegmentationCompositeServiceImpl implements SegmentationCompositeService {
    public static final int PLATE_SIZE = 150;
    private PlateSegmentationService plateSegmentationService;
    private NormalizationService normalizationService;
    private SignsSegmentationService signsSegmentationService;
    private MainWindow mainWindow;

    @Override
    public List<List<Mat>> segmentSign(Mat image) {
        List<Mat> plates = plateSegmentationService.segmentPlatesFromImage(image);
        List<Mat> normalizedPlates = new ArrayList<>();
        List<List<Mat>> allSigns = new ArrayList<>();
        for (Mat plate : plates) {
            Mat normalizePlate = normalizationService.normalizePlate(plate);
            normalizedPlates.add(normalizePlate);
            setPlateOnWindow(normalizePlate);
        }
        for (Mat normalizedPlate : normalizedPlates) {
            List<Mat> separateSigns = signsSegmentationService.separateSigns(normalizedPlate);
            setSigns(separateSigns);
            allSigns.add(separateSigns);
        }
        return allSigns;
    }

    @Override
    public void setMainWindow(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }

    public void setPlateOnWindow(Mat image) {
        ImageUtils.resizeImage(image, PLATE_SIZE);
        mainWindow.getLblPlate3().setIcon(mainWindow.getLblPlate2().getIcon());
        mainWindow.getLblPlate2().setIcon(mainWindow.getLblPlate1().getIcon());
        ImageUtils.displayImageOnFrame2(ImageUtils.mat2BufferedImage(image), mainWindow.getFrame(), mainWindow.getLblPlate1()
        );
    }

    public void setSigns(List<Mat> signs) {
        for (int i = 0; i < 9; i++) {
            mainWindow.getSigns3().get(i).setIcon(mainWindow.getSigns2().get(i).getIcon());
            mainWindow.getSigns2().get(i).setIcon(mainWindow.getSigns1().get(i).getIcon());
            mainWindow.getSigns1().get(i).setIcon(null);
        }
        if (signs != null) {
            for (int i = 0; i < signs.size(); i++) {
                ImageUtils.displayImageOnFrame2(ImageUtils.mat2BufferedImage(signs.get(i)), mainWindow.getFrame(), mainWindow.getSigns1().get(i));
            }
        }

    }

    // Spring injections
    public void setPlateSegmentationService(PlateSegmentationService plateSegmentationService) {
        this.plateSegmentationService = plateSegmentationService;
    }

    public void setNormalizationService(NormalizationService normalizationService) {
        this.normalizationService = normalizationService;
    }

    public void setSignsSegmentationService(SignsSegmentationService signsSegmentationService) {
        this.signsSegmentationService = signsSegmentationService;
    }
}
