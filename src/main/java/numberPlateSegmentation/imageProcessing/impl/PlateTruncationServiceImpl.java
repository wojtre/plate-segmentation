package numberPlateSegmentation.imageProcessing.impl;

import numberPlateSegmentation.imageProcessing.interfaces.PlateTruncationService;
import numberPlateSegmentation.imageProcessing.interfaces.ProjectionService;
import org.opencv.core.Mat;
import utils.ChartUtils;

/**
 * Created by Wojciech on 01-Nov-15.
 */
public class PlateTruncationServiceImpl implements PlateTruncationService {
    public static final int H = 6;
    private ProjectionService projectionService;

    @Override
    public Mat truncatePlate(Mat plate) {

        Mat horizontallyPlate = truncateHorizontallyPlate(plate);
        return horizontallyPlate;
    }

    private Mat truncateHorizontallyPlate(Mat plate) {
        long[] projectOnAxisY = projectionService.projectOnAxisY(plate);
        ChartUtils.printProjectionYFromArray(projectOnAxisY);
        long[] derivative = countDerivative(projectOnAxisY);
//        ChartUtils.printProjectionYFromArray(derivative);
        int minIndex = findMinIndex(derivative);
        int maxIndex = findMaxIndex(derivative);
        int tmp;
        if (minIndex < maxIndex) {
            tmp = maxIndex;
            maxIndex = minIndex;
            minIndex = tmp;
        }
        Mat lines = new Mat();
        plate.copyTo(lines);
//        ImageUtils.displayImage(ImageUtils.printHorizontalLines(lines, Arrays.asList(maxIndex, minIndex)));
//        Highgui.imwrite("C:\\Users\\Wojciech\\Google Drive\\studia\\inz\\images\\segmentacja_rzut\\przycinanie_linie.jpg", ImageUtils.printHorizontalLines(lines, Arrays.asList(maxIndex, minIndex)));

        Mat submat = plate.submat(maxIndex, minIndex, 0, plate.cols() - 1);
//        ImageUtils.displayImage(submat);
        return submat;
    }

    private long[] countDerivative(long[] projection) {
        long[] derivative = new long[projection.length];
        for (int i = 0; i < projection.length; i++) {
            derivative[Math.max(i - H / 2, 0)] = projection[i] - projection[Math.max(0, i - H)];
        }
        return derivative;
    }

    private int findMaxIndex(long[] data) {
        long maxValue = 0;
        int maxIndex = 0;
        for (int i = 0; i < data.length; i++) {
            if (data[i] > maxValue) {
                maxIndex = i;
                maxValue = data[i];
            }
        }
        return maxIndex;
    }

    private int findMinIndex(long[] data) {
        long minValue = 0;
        int minIndex = 0;
        for (int i = 0; i < data.length; i++) {
            if (data[i] < minValue) {
                minIndex = i;
                minValue = data[i];
            }
        }
        return Math.max(minIndex - H / 2, 0);
    }
    //Spring injections

    public void setProjectionService(ProjectionService projectionService) {
        this.projectionService = projectionService;
    }
}
