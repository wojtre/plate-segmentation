package numberPlateSegmentation.imageProcessing.interfaces;

import org.opencv.core.Mat;

import java.util.List;

/**
 * Created by Wojciech on 10-Oct-15.
 */
public interface ProjectionService {
    List<Integer> findPlacesOfPartition(Mat plate);

    List<Integer> findPlacesOfTruncation(long[] deriveration);

    long[] projectOnAxisXBinary(Mat mat);

    long[] projectOnAxisX(Mat mat);

    int[] projectOnAxisYBinary(Mat mat);

    long[] projectOnAxisY(Mat mat);
}
