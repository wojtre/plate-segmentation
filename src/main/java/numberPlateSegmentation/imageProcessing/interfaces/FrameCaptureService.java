package numberPlateSegmentation.imageProcessing.interfaces;

import numberPlateSegmentation.GUI.MainWindow;

/**
 * Created by Wojciech on 10-Oct-15.
 */
public interface FrameCaptureService {
    void run();

    @SuppressWarnings("deprecation")
    void capture(String filename);

    void setMainWindow(MainWindow mainWindow);
}
