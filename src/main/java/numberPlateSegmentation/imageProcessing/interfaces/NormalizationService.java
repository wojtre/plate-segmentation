package numberPlateSegmentation.imageProcessing.interfaces;

import org.opencv.core.Mat;

/**
 * Created by Wojciech on 06-Oct-15.
 */
public interface NormalizationService {

    Mat normalizePlate(Mat plate);
}
