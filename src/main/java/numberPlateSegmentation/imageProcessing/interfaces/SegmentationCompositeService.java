package numberPlateSegmentation.imageProcessing.interfaces;

import numberPlateSegmentation.GUI.MainWindow;
import org.opencv.core.Mat;

import java.util.List;

/**
 * Created by Wojciech on 31-Oct-15.
 */
public interface SegmentationCompositeService {
    List<List<Mat>> segmentSign(Mat image);

    void setMainWindow(MainWindow mainWindow);
}
