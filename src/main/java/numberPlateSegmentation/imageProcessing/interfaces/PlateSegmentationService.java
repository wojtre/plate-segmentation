package numberPlateSegmentation.imageProcessing.interfaces;

import numberPlateSegmentation.GUI.MainWindow;
import org.opencv.core.Mat;

import javax.swing.*;
import java.util.List;

/**
 * Created by Wojciech on 06-Oct-15.
 */
public interface PlateSegmentationService {
    List<Mat> segmentPlatesFromImage(Mat image);

    void setMainWindow(MainWindow mainWindow);

    void setPanel(JPanel panel);
}
