/**
 * Created by Wojciech on 05-Oct-15.
 */

import numberPlateSegmentation.imageProcessing.impl.NormalizationServiceImpl;
import numberPlateSegmentation.imageProcessing.interfaces.SignsSegmentationService;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import utils.ImageUtils;

public class Main {

    public static final String OPENCV_PATH = "C:\\Program Files\\Java\\opencv-2.4.11\\opencv\\build\\java\\x64\\opencv_java2411.dll";

    public static void main(String[] args) {
        System.load(OPENCV_PATH);
        ApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("context.xml");
//        FrameCaptureService frameCaptureService = classPathXmlApplicationContext.getBean("frameCaptureService",
//                FrameCaptureService.class);
//        long start = System.nanoTime();
//        frameCaptureService.capture("C:\\Users\\Wojciech\\javaWorkspace\\numberPlateRecognition\\filmy\\film2.avi");
//        long end = System.nanoTime();
//        System.out.println((end-start)/1000000000.0+" s");

        Mat imread = Highgui.imread("C:\\Users\\Wojciech\\IdeaProjects\\plate-segmentation\\src\\main\\resources\\tablice\\tablica41.jpg", Highgui.CV_LOAD_IMAGE_GRAYSCALE);

//        Mat imread = Highgui.imread("C:\\Users\\Wojciech\\IdeaProjects\\plate-segmentation\\src\\main\\resources\\cars\\test3.jpg", Highgui.CV_LOAD_IMAGE_GRAYSCALE);
//                NormalizationServiceImpl normalizationService = new NormalizationServiceImpl();
//        PlateTruncationService plateTruncationService = classPathXmlApplicationContext.getBean("plateTruncationService", PlateTruncationService.class);
//        Mat normalizedPlate = normalizationService.normalizePlate(imread);
//        normalizedPlate = plateTruncationService.truncatePlate(normalizedPlate);
//        ContourSegmentationServiceImpl contourSegmentationService = new ContourSegmentationServiceImpl();
//        long start = System.nanoTime();
//        contourSegmentationService.separateSigns(normalizedPlate);
//        long end = System.nanoTime();
//        System.out.println("\n"+(end - start) / 1000000.0 + " ms\n");


        NormalizationServiceImpl normalizationService = new NormalizationServiceImpl();
        Mat normalizedPlate = normalizationService.normalizePlate(imread);
        SignsSegmentationService signsSegmentationService = classPathXmlApplicationContext.getBean("contourSegmentationService", SignsSegmentationService.class);
        java.util.List<Mat> plate = signsSegmentationService.separateSigns(normalizedPlate);
        int i = 0;
        for (Mat im : plate) {

            ImageUtils.displayImage(im);
            Highgui.imwrite("C:\\Users\\Wojciech\\Google Drive\\studia\\inz\\images\\segmentacja_grupowanie\\znak" + i + ".jpg", im);
            i++;
        }


//        PlateSegmentationServiceImpl plateSegmentationService = new PlateSegmentationServiceImpl();
//        plateSegmentationService.segmentPlatesFromImage(imread);

    }
}
